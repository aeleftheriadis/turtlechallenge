Task created using .Net Core v1.1 alongside with some unit tests with xUnit.

This application is supposed to accept two files with json extension you can check the included json files.

I have created 3 services one for file read, the second for validation of inputs and command line arguments and the third for the execution of moves.

I have added abstraction using Dependency Injection.

My dependencies are Newtonsoft.Json Microsoft.Extensions.DependencyInjection

The application was developed using Visual Studio 2017 15.3.2 on Windows10 x64.

In order to create an executable in .Net Core you should publish the executable.