﻿using System;
using System.Collections.Generic;
using System.Text;
using TurtleChallenge.Models;

namespace TurtleChallenge.Services.Contracts
{
    public interface IValidatorService
    {
        string ValidateInput(string[] args);

        string ValidateInitialSettings(Settings Settings);
    }
}
