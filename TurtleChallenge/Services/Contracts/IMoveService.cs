﻿using System;
using System.Collections.Generic;
using System.Text;
using TurtleChallenge.Models;

namespace TurtleChallenge.Services.Contracts
{
    public interface IMoveService
    {
        void ExecuteSequence(Settings initialSettings, MoveSequence moveSequence, Turtle turtle);
    }
}
