﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TurtleChallenge.Services.Contracts
{
    public interface IFileService
    {
        object Read<T>(string fileName);
    }
}
