﻿using System;
using System.Collections.Generic;
using System.Text;
using TurtleChallenge.Enums;
using TurtleChallenge.Models;
using TurtleChallenge.Services.Contracts;

namespace TurtleChallenge.Services.Implementations
{
    public class MoveService : IMoveService
    {
        public void ExecuteSequence(Settings initialSettings, MoveSequence moveSequence, Turtle turtle)
        {
            int sequenceCount = 1;
            foreach (var movement in moveSequence.moves)
            {
                Console.Write(string.Format("Sequence {0}: ", sequenceCount++));
                if (movement == Move.r)
                {
                    turtle.direction = CalculateNewDirection(turtle.direction);
                    Console.WriteLine("Still in danger!");
                }
                else
                {
                    turtle.currentPosition = CalculateCurrentPosition(initialSettings, turtle.currentPosition, turtle.direction);
                }
            }
        }

        private Direction CalculateNewDirection(Direction oldDirection)
        {
            if (oldDirection == Direction.West)
                return Direction.North;
            else
            {
                oldDirection++;
                return oldDirection;
            }
        }

        private Position CalculateCurrentPosition(Settings initialSettings, Position position, Direction direction)
        {
            var xPosition = position.xPosition;
            var yPosition = position.yPosition;
            var newPosition = new Position() { xPosition = xPosition, yPosition = yPosition };
            switch (direction)
            {
                case Direction.North:
                    newPosition.yPosition--;
                    break;
                case Direction.East:
                    newPosition.xPosition++;
                    break;
                case Direction.South:
                    newPosition.yPosition++;
                    break;
                case Direction.West:
                    newPosition.xPosition--;
                    break;
            }

            if (CheckForWallHit(newPosition, initialSettings.board.size))
            {
                Console.WriteLine("You hit the wall! Can't move");
                return position;
            }
            else if (CheckForMineHit(initialSettings.mines, newPosition))
            {
                Console.WriteLine("Mine hit!");
            }
            else if (CheckForExit(initialSettings.exitPoint, newPosition))
            {
                Console.WriteLine("Success!");
            }
            else
            {
                Console.WriteLine("Still in danger!");
            }

            return newPosition;
        }

        private bool CheckForExit(Position exitPoint, Position position)
        {
            return exitPoint.xPosition == position.xPosition && exitPoint.yPosition == position.yPosition;
        }

        private bool CheckForMineHit(List<Position> mines, Position position)
        {
            return mines.Find(x => (x.xPosition == position.xPosition) &&
            (x.yPosition == position.yPosition)) != null;
        }

        private bool CheckForWallHit(Position position, BoardSize size)
        {
            return position.xPosition > size.xDimension - 1 ||
                position.yPosition > size.yDimension - 1 ||
                position.yPosition < 0 ||
                position.xPosition < 0;
        }
    }
}
