﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using TurtleChallenge.Services.Contracts;

namespace TurtleChallenge.Services.Implementations
{
    public class JsonFileService : IFileService
    {
        public object Read<T>(string fileName)
        {
            using (var stream = new FileStream(fileName, FileMode.Open))
            using (var reader = new StreamReader(stream))
            {
                string json = reader.ReadToEnd();                
                return JsonConvert.DeserializeObject<T>(json);
            }
        }
    }
}
