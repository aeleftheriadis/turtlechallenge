﻿using System;
using System.Collections.Generic;
using System.Text;
using TurtleChallenge.Models;
using TurtleChallenge.Services.Contracts;

namespace TurtleChallenge.Services.Implementations
{
    public class ValidatorService: IValidatorService
    {
        public string ValidateInput(string[] args)
        {
            if (args?.Length != 2)
            {
                return "Please provide 2 files names, one for game settings and one for sequense of moves";
            }
            return null;            
        }

        public string ValidateInitialSettings(Settings settings)
        {
            var maxX = settings.board.size.xDimension-1;
            var maxY = settings.board.size.yDimension-1;

            var invalidSettingsDetected = settings.mines.Find(x => (x.xPosition > maxX) && (x.yPosition > maxY)) != null ||
                settings.exitPoint.xPosition > maxX || settings.exitPoint.yPosition > maxY ||
                settings.startPosition.xPosition > maxX || settings.startPosition.yPosition > maxY;

            if (invalidSettingsDetected)
            {
                return "Invalid game settings, please check the provided values";
            }
            return null;
        }
    }
}
