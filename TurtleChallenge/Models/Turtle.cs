﻿using System;
using System.Collections.Generic;
using System.Text;
using TurtleChallenge.Enums;

namespace TurtleChallenge.Models
{
    public class Turtle
    {
        public Position currentPosition { get; set; }
        public Direction direction { get; set; }
    }
}
