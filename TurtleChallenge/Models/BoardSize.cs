﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TurtleChallenge.Models
{
    public class BoardSize
    {
        [Range((int)0.0, int.MaxValue)]
        public int xDimension { get; set; }
        [Range((int)0.0, int.MaxValue)]
        public int yDimension { get; set; }
    }
}
