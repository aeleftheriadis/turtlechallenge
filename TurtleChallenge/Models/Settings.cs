﻿using System;
using System.Collections.Generic;
using System.Text;
using TurtleChallenge.Enums;

namespace TurtleChallenge.Models
{
    public class Settings
    {
        public Board board { get; set; }
        public Position startPosition { get; set; }
        public Direction direction { get; set; }
        public Position exitPoint { get; set; }
        public List<Position> mines { get; set; }
    }
}
