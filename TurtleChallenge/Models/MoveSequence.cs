﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TurtleChallenge.Enums;

namespace TurtleChallenge.Models
{
    public class MoveSequence
    {        
        public List<Move> moves { get; set; }
    }
}
