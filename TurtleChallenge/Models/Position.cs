﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TurtleChallenge.Models
{
    public class Position
    {
        [Range((int)0.0, int.MaxValue)]
        public int xPosition { get; set; }
        [Range((int)0.0, int.MaxValue)]
        public int yPosition { get; set; }
    }
}
