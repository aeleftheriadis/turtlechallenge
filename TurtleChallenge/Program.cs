﻿using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using TurtleChallenge.Enums;
using TurtleChallenge.Models;
using TurtleChallenge.Services;
using TurtleChallenge.Services.Contracts;
using TurtleChallenge.Services.Implementations;

namespace TurtleChallenge
{
    class Program
    {
        static void Main(string[] args)
        {
            var serviceProvider = new ServiceCollection()
                .AddSingleton<IValidatorService, ValidatorService>()
                .AddSingleton<IFileService, JsonFileService>()
                .AddSingleton<IMoveService, MoveService>()
                .BuildServiceProvider();

            var validator = serviceProvider.GetService<IValidatorService>();
            var validationResult = validator.ValidateInput(args);
            if (!string.IsNullOrEmpty(validationResult))
            {
                Console.WriteLine(validationResult);
                return;
            }

            var jsonFile = serviceProvider.GetService<IFileService>();
            if (!File.Exists(string.Format("{0}.json", args[0])) || !File.Exists(string.Format("{0}.json", args[1])))
            {
                Console.WriteLine("One or more files don't exist");
                return;
            }
            var initialSettings = (Settings)jsonFile.Read<Settings>(string.Format("{0}.json", args[0]));
            var moveSequence = (MoveSequence)jsonFile.Read<MoveSequence>(string.Format("{0}.json", args[1]));

            validationResult = validator.ValidateInitialSettings(initialSettings);
            if (!string.IsNullOrEmpty(validationResult))
            {
                Console.WriteLine(validationResult);
                return;
            }

            Turtle turtle = new Turtle()
            {
                currentPosition = initialSettings.startPosition,
                direction = initialSettings.direction
            };

            var moveService = serviceProvider.GetService<IMoveService>();
            moveService.ExecuteSequence(initialSettings, moveSequence, turtle);
        }
    }
}