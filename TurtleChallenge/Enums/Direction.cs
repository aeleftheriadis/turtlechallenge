﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Text;

namespace TurtleChallenge.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum Direction
    {
        [EnumMember(Value = "North")]
        [Description("North")]
        North,
        [EnumMember(Value = "East")]
        [Description("East")]
        East,
        [EnumMember(Value = "South")]
        [Description("South")]
        South,
        [EnumMember(Value = "West")]
        [Description("West")]
        West
    }
}
