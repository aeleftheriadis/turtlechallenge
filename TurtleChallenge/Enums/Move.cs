﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Text;

namespace TurtleChallenge.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum Move
    {
        [EnumMember(Value = "m")]
        [Description("m")]
        m,
        [EnumMember(Value = "r")]
        [Description("r")]
        r
    }
}
