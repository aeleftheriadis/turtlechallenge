using System;
using TurtleChallenge.Services.Implementations;
using Xunit;

namespace TurtleChallenge.Tests
{
    public class ValidatorTest
    {
        private readonly ValidatorService _validatorService;

        public ValidatorTest()
        {
            _validatorService = new ValidatorService();
        }

        [Fact]
        public void ValidateInput_LessThanTwoArgs_False()
        {
            //Arrange
            var args = new string[] { "game-setting" };

            //Act
            var result = _validatorService.ValidateInput(args);

            //Assert
            Assert.False(string.IsNullOrEmpty(result));
        }

        [Fact]
        public void ValidateInput_MoreThanTwoArgs_False()
        {
            //Arrange
            var args = new string[] { "game-setting", "moves", "moves0"};

            //Act
            var result = _validatorService.ValidateInput(args);

            //Assert
            Assert.False(string.IsNullOrEmpty(result));
        }

        [Fact]
        public void ValidateInput_ExactTwoArguments_True()
        {
            //Arrange
            var args = new string[] { "game-setting", "moves" };

            //Act
            var result = _validatorService.ValidateInput(args);

            //Assert
            Assert.True(string.IsNullOrEmpty(result));
        }
    }
}
