﻿using System;
using System.Collections.Generic;
using System.Text;
using TurtleChallenge.Models;
using TurtleChallenge.Services.Implementations;
using Xunit;

namespace TurtleChallenge.Tests
{
    public class MoveServiceTest
    {
        private readonly MoveService _moveService;

        public MoveServiceTest()
        {
            _moveService = new MoveService();
        }

        [Fact]
        public void ExecuteSequence_EmptyArguments_ThrowObjectNullReferenceException()
        {
            //Arrange
            var settings = new Settings();
            var moveSequence = new MoveSequence();
            var turtle = new Turtle();

            //Act
            var exception = Record.Exception(() => _moveService.ExecuteSequence(settings, moveSequence, turtle));

            //Assert
            Assert.IsType(typeof(NullReferenceException), exception);
        }

    }
}
